from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import cfscrape
from lxml import html
import sys
import os

currentdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append("/home/manage_report")

from Send_report.mywrapper import magicDB


class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_content()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def prepare_item_date(self, date):
        months = {
            'января': '01',
            'февраля': '02',
            'марта': '03',
            'апреля': '04',
            'мая': '05',
            'июня': '06',
            'июля': '07',
            'августа': '08',
            'сентября': '09',
            'октября': '10',
            'ноября': '11',
            'декабря': '12',
        }
        day = date.split(' ')[0]
        month = date.split(' ')[1]
        year = date.split(' ')[2]
        month = month.replace(month, months[month]).strip()
        date = '.'.join([day, month, year])

        return date

    def get_urls(self):
        page = 1

        date_format = '%d.%m.%Y'
        today = datetime.now()
        yesterday = today - timedelta(days=1)
        tomorrow = today + timedelta(days=1)
        fromdate = yesterday.strftime(date_format)
        todate = tomorrow.strftime(date_format)

        print('from: {}, to: {}'.format(fromdate, todate))

        self.session.headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 Edg/108.0.1462.54',
        }
        ads = []

        while page < 2:
            url = f'https://estp.ru/purchases?section=e-shop&section=commercial&section=prequalification&page={page}'
            response = self.session.get(url, timeout=60)
            soup = BeautifulSoup(response.text, 'lxml')

            for items in soup.find_all('div', class_='Card_card__ERPiY ListItem_card__fVb4K'):
                new_urls = items.find('a', class_='Title_link__YxKXz').get('href')
                lot_url = 'https://estp.ru/' + new_urls
                data = ''.join(items.find('div', class_='PublishDate_publishDate__cAvqX').get_text()).split('Опубликовано ')[1]
                new_date = self.prepare_item_date(data)

                r_date = datetime.strptime(new_date, '%d.%m.%Y')
                date_f = datetime.strptime(fromdate, '%d.%m.%Y')
                date_t = datetime.strptime(todate, '%d.%m.%Y')

                if r_date > date_f and r_date < date_t:
                    if new_urls not in ads:
                        ads.append(lot_url)
            page += 1
        return ads

    def get_content(self):
        ads = self.get_urls()
        contents = []
        for link in ads:
            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': '',
                'url': '',
                'lots': [],
                'attachments': [],
                'procedureInfo': {
                    'endDate': '',
                    'scoringDate': ''
                },

                'customer': {
                    'fullName': '',
                    'factAddress': '',
                    'inn': 0,
                    'kpp': 0,
                },
                'contactPerson': {
                    'lastName': '',
                    'firstName': '',
                    'middleName': '',
                    'contactPhone': '',
                    'contactEMail': '',
                }
            }

            try:
                response = self.session.get(link, timeout=30)
                tree = html.document_fromstring(response.content)

                lots_link = link.replace('main', 'lots')
                contacts_link = link.replace('main', 'customer')
                attach_link = link.replace('main', 'docs')

                lots_response = self.session.get(lots_link, timeout=30)
                lots_tree = html.document_fromstring(lots_response.content)

                contact_response = self.session.get(contacts_link, timeout=30)
                contact_tree = html.document_fromstring(contact_response.content)

                attach_response = self.session.get(attach_link, timeout=30)
                attach_tree = html.document_fromstring(attach_response.content)

                item_data['title'] = str(self.get_title(tree))
                item_data['url'] = str(link)
                item_data['purchaseType'] = str(self.get_type(tree))
                item_data['purchaseNumber'] = str(self.get_number(tree))

                item_data['customer']['fullName'] = str(self.get_customer(tree))
                item_data['customer']['factAddress'] = str(self.get_adress(contact_tree))
                item_data['customer']['inn'] = int(self.get_inn(contact_tree))

                last_name, first_name, middle_name = self.get_names(contact_tree)
                item_data['contactPerson']['lastName'] = str(last_name)
                item_data['contactPerson']['firstName'] = str(first_name)
                item_data['contactPerson']['middleName'] = str(middle_name)

                item_data['contactPerson']['contactPhone'] = str(self.get_phone(contact_tree))
                item_data['contactPerson']['contactEMail'] = str(self.get_email(contact_tree))

                item_data['procedureInfo']['endDate'] = self.get_end_date(tree)
                item_data['procedureInfo']['scoringDate'] = self.get_scor_date(tree)

                item_data['lots'] = self.get_lots(lots_tree)

                item_data['attachments'] = self.get_attach(attach_tree)

                contents.append(item_data)

            except:
                print("Страница с ошибкой ", link)
        return contents

    def get_title(self, tree):
        try:
            data = ''.join(tree.xpath('//h2/text()[1]')).strip()
        except:
            data = ''
        return data

    def get_type(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Способ закупки")]/parent::*/./*[10]/text()')).strip()
        except:
            data = ''
        return data

    def get_number(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Код закупки")]/parent::*/./*[2]/text()')).strip()
        except:
            data = ''
        return data

    def get_customer(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Организатор")]/parent::*/./*[8]/a/text()')).strip()
        except:
            data = ''
        return data

    def get_adress(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Юридический адрес")]/parent::*/./*[4]/text()')).strip()
        except:
            data = ''
        return data

    def get_inn(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"ИНН")]/parent::*/./*[2]/text()')).strip()
            if not data or data is None:
                data = 0
        except:
            data = 0
        return data

    def get_names(self, tree):
        try:
            contact = ''.join(
                tree.xpath('//*[contains(text(),"Контактные лица")]/parent::*/./*[3]/./*[1]/h4/text()')).strip()

            try:
                last_name = ''.join(contact.split()[0])
            except:
                last_name = ''
            try:
                first_name = ''.join(contact.split()[1])
            except:
                first_name = ''
            try:
                middle_name = ''.join(contact.split()[2])
            except:
                middle_name = ''
        except:
            last_name = ''
            first_name = ''
            middle_name = ''
        return last_name, first_name, middle_name

    def get_phone(self, tree):
        try:
            data = ''.join(tree.xpath(
                '//*[contains(text(),"Контактные лица")]/parent::*/./*[3]/./*[1]/./*[2]/./*[2]/text()')).split(' доб.')[
                       0] or ''.join(
                tree.xpath('//*[contains(text(),"Контактные лица")]/parent::*/./*[3]/./*[1]/./*[2]/./*[2]/text()'))
        except:
            data = ''
        return data

    def get_email(self, tree):
        try:
            data = ''.join(
                tree.xpath('//*[contains(text(),"Контактные лица")]/parent::*/./*[3]/./*[1]/./*[2]/./*[4]/text()'))
        except:
            data = ''
        return data

    def get_end_date(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Прием публичных предложений")]/parent::*/./*[2]/text()[1]')).strip()[19:35] or ''.join(tree.xpath('//*[contains(text(),"Приём заявок")]/parent::*/./*[2]/text()[1]')).strip()[19:35]
            formatted_date = self.formate_date(data)
        except:
            formatted_date = None
        return formatted_date

    def get_scor_date(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Подведение итогов")]/parent::*/./*[2]/text()[1]')).strip()[19:35] or ''.join(tree.xpath('//*[contains(text(),"Рассмотрение заявок")]/parent::*/./*[2]/text()[1]')).strip()[19:35]
            formatted_date = self.formate_date(data)
        except:
            formatted_date = None
        return formatted_date

    def get_lots(self, tree):
        try:
            data = []
            lots = {
                'region': '',
                'address': '',
                'price': '',
                'lotItems': []

            }
            names = {'code': ' '.join(''.join(tree.xpath('//*[contains(text(),"Лот ")]/text()[2]')).split()),
                     'name': ' '.join(''.join(
                             tree.xpath('//*[contains(text(),"Наименование лота")]/parent::*/./*[2]/text()')).split())}

            lots['address'] = ' '.join(
                ''.join(tree.xpath('//*[contains(text(),"Адрес")]/parent::*/./*[4]/text()')).strip().split())
            lots['price'] = ' '.join(
                ''.join(tree.xpath('//*[contains(text(),"НМЦ лота")]/parent::*/./*[4]/text()')).strip().split())
            lots['lotItems'].append(names)
            data.append(lots)
        except:
            data = []
        return data

    def get_attach(self, tree):
        try:
            data = []
            doc_div = tree.xpath('//ol[@class="DocsPurchaseTab_list__WPnh_"]/li[@class="DocsPurchaseTab_item__DhWRh"]')

            for doc in doc_div:
                docs = {'docDescription': ''.join(doc.xpath('./*[2]/a/text()')),
                        'url': 'https://estp.ru/' + ''.join(doc.xpath('./*[4]/@href'))}

                data.append(docs)
        except:
            data = []
        return data

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%Y %H:%M')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
